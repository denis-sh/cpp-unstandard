/*
Array with reference semantics implementation for C++.

Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <algorithm> // for `move`, `swap`
#include <initializer_list>
#include <new> // for `nothrow`
#ifdef UNSTD_UNITTEST
#include <string>
#endif

#include "internal/bounds_check.hpp"
#include "internal/common.hpp"
#include "internal/elementwise.hpp"
#include "internal/prefixed_buffer.hpp"
#include "internal/type_traits.hpp" // for `enable_if_t`, `is_same_v`, `remove_const_t`
#include "internal/utils.hpp"


namespace unstd
{

template<typename T>
struct Array final
{
private:

	// Private `RCPtr` struct
	// --------------------------------------------------

	using MutableT = _i6l::remove_const_t<T>;

	struct RCPtr
	{
	private:

		_i6l::PrefixedBuffer<ssize, T> _buff;

		bool isNull() const noexcept
		{
			return !_buff.allocated();
		}

		ssize& refCount() noexcept
		{
			return _buff.prefix();
		}

	public:

		explicit RCPtr() = default;

		explicit RCPtr(const T* const ptr, const ssize count, T*& buff)
			noexcept(_i6l::Elementwise<MutableT>::copyConstructNothrow)
		{
			buff = _buff.allocate(count).buffer;
			refCount() = 1;
			try
			{
				_i6l::Elementwise<MutableT>::copyConstruct(const_cast<MutableT*>(buff), ptr, count);
			}
			catch(...)
			{
				_buff.free();

#ifdef UNSTD_GNU_CXX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wterminate"
#endif
				throw;
#ifdef UNSTD_GNU_CXX
#pragma GCC diagnostic pop
#endif
			}
		}

		// Note: copy and move constructors are explicit, so
		// `RCPtr ptr = otherPtr;` expression is not valid.

		explicit RCPtr(RCPtr& other) noexcept :
			_buff(other._buff)
		{
			INTERNAL_UNSTD_ASSERT(isNull() || refCount() > 0);

			if(!isNull())
				++refCount();
		}

		explicit RCPtr(RCPtr&& other) noexcept
		{
			_buff.swap(other._buff);
		}

		void destruct(T* const ptr, const ssize count)
			noexcept(_i6l::Elementwise<T>::destructNothrow)
		{
			INTERNAL_UNSTD_ASSERT(isNull() || refCount() > 0);

			if(!isNull() && --refCount() == 0)
			{
				try
				{
					_i6l::Elementwise<T>::destruct(ptr, count);
				}
				catch(...)
				{
					_buff.free();

#ifdef UNSTD_GNU_CXX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wterminate"
#endif
					throw;
#ifdef UNSTD_GNU_CXX
#pragma GCC diagnostic pop
#endif
				}
				_buff.free();
			}
		}

		void swap(RCPtr& other) noexcept
		{
			_buff.swap(other._buff);
		}
	};

	// Fields
	// --------------------------------------------------

	T* _ptr;
	ssize _length;
	mutable RCPtr _rc;

	// Constructors and destructor
	// --------------------------------------------------

#define implicit UNSTD_IMPLICIT

	/* Note:
	There is no default constructor because of the following reason:
	Generally it's better to explicitly require initialization instead of providing default
	constructor. But anyway it's impossible to initialize an array as "null" by default and
	as "empty" via `{}` because if braced-init-list is empty during list initialization
	default constructor is called thus these cases can't be distinguished.
	*/

	explicit Array(T* const ptr, const ssize length, RCPtr& rc) noexcept :
		_ptr(ptr),
		_length(length),
		_rc(rc)
	{
	}

public:

	implicit Array(decltype(nullptr)) noexcept :
		_ptr(nullptr),
		_length(0)
	{
	}

	implicit Array(std::initializer_list<T> initializerList)
		noexcept(_i6l::Elementwise<MutableT>::copyConstructNothrow) :
		_length(ssize(initializerList.size())), // No overflow check here, it is performed in the line below
		_rc(initializerList.begin(), _i6l::checkedElementCountAsSigned(initializerList.size()), _ptr)
	{
	}

	/* Note:
	An array of non-const objects can't be constructed from `const` array
	with the same element type because it will lose `const`.
	As a result even `const` array can't be constructed from `const` array
	because there is no information in the constructor regarding resulting
	object qualifiers.
	*/

#ifndef UNSTD_VISUAL_CXX
	implicit Array(Array& other) = default;
#else
	// Workaround Visual C++ bug (present in VS 2015 Update 3):
	// 2x "Error C2580 multiple versions of a defaulted special member functions are not allowed"
	// Issue URL: https://connect.microsoft.com/VisualStudio/feedback/details/1374613/vc14-rc-incorrect-compiler-error-c2580

	implicit Array(Array& other) noexcept :
		_ptr(other._ptr),
		_length(other._length),
		_rc(other._rc)
	{
	}
#endif

	friend struct Array<const T>;
	friend struct Array<_i6l::remove_const_t<T>>;

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<T, const U>>>
	implicit Array(const Array<U>& other) noexcept :
		_ptr(other._ptr),
		_length(other._length),
		_rc(reinterpret_cast<RCPtr&>(other._rc))
	{
	}

	implicit Array(Array&& other) noexcept :
		_ptr(other._ptr),
		_length(other._length),
		_rc(std::move(other._rc))
	{
		other._ptr = nullptr;
		other._length = 0;
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<T, const U>>>
	implicit Array(Array<U>&& other) noexcept :
		_ptr(other._ptr),
		_length(other._length),
		_rc(std::move(reinterpret_cast<RCPtr&>(other._rc)))
	{
		other._ptr = nullptr;
		other._length = 0;
	}

#undef implicit

	~Array() noexcept(_i6l::Elementwise<T>::destructNothrow)
	{
		_rc.destruct(_ptr, _length);
	}

	// Properties
	// --------------------------------------------------

	T* ptr() noexcept
	{
		return _ptr;
	}

	const T* ptr() const noexcept
	{
		return _ptr;
	}

	ssize length() const noexcept
	{
		return _length;
	}

	// Element access by index
	// --------------------------------------------------

	T& operator[](const ssize index) noexcept
	{
		_i6l::checkElementIndex(index, _length);
		return _ptr[index];
	}

	const T& operator[](const ssize index) const noexcept
	{
		_i6l::checkElementIndex(index, _length);
		return _ptr[index];
	}

	// Assignment operators & swap function
	// --------------------------------------------------

	Array& operator=(decltype(nullptr)) & noexcept(noexcept(Array(nullptr)))
	{
		Array tmp = nullptr;
		swap(tmp);
		return *this;
	}

	Array& operator=(std::initializer_list<T> initializerList) & noexcept(noexcept(Array(initializerList)))
	{
		Array tmp = initializerList;
		swap(tmp);
		return *this;
	}

	Array& operator=(Array& other) & noexcept(noexcept(Array(other)))
	{
		Array tmp = other;
		swap(tmp);
		return *this;
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<T, const U>>>
	Array& operator=(const Array<U>& other) & noexcept(noexcept(Array(other)))
	{
		Array tmp = other;
		swap(tmp);
		return *this;
	}

	/* Note: In moving assignment operators don't just call
	`swap(other)` but also set `other` as "null".
	*/

	Array& operator=(Array&& other) & noexcept(noexcept(Array(std::move(other))))
	{
		Array tmp = std::move(other);
		swap(tmp);
		return *this;
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<T, const U>>>
	Array& operator=(Array<U>&& other) & noexcept(noexcept(Array(std::move(other))))
	{
		Array tmp = std::move(other);
		swap(tmp);
		return *this;
	}

	void swap(Array& other) noexcept
	{
		std::swap(_ptr, other._ptr);
		std::swap(_length, other._length);
		_rc.swap(other._rc);
	}

	// Comparison operators & functions
	// --------------------------------------------------

	bool is(const Array& other) const noexcept
	{
		return _ptr == other._ptr && _length == other._length;
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<const T, const U>>>
	bool is(const Array<U>& other) const noexcept
	{
		return _ptr == other._ptr && _length == other._length;
	}

#ifdef UNSTD_VISUAL_CXX
#define DEPENDENT_FALSE false
#else
#define DEPENDENT_FALSE !_i6l::is_same_v<T, T>
#endif

	bool operator==(decltype(nullptr)) const noexcept
	{
		static_assert(DEPENDENT_FALSE, "Use `arr.is(nullptr)` to check if array is 'null' instead of `arr == nullptr`.");
	}

	bool operator!=(decltype(nullptr)) const noexcept
	{
		static_assert(DEPENDENT_FALSE, "Use `!arr.is(nullptr)` to check if array is not 'null' instead of `arr != nullptr`.");
	}

#undef DEPENDENT_FALSE

	bool operator==(const Array& other) const noexcept(_i6l::Elementwise<T>::equalNothrow)
	{
		return _i6l::Elementwise<T>::equal(_ptr, _length, other._ptr, other._length);
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<const T, const U>>>
	bool operator==(const Array<U>& other) const noexcept(_i6l::Elementwise<const T>::equalNothrow)
	{
		return _i6l::Elementwise<const T>::equal(_ptr, _length, other._ptr, other._length);
	}

#ifdef UNSTD_GNU_CXX
// Workaround GNU C++ compiler bug 78301 in noexcept specification
// Bug URL: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=78301
#define NOEXCEPT_THIS Array(nullptr).
#else
#define NOEXCEPT_THIS
#endif

	bool operator!=(const Array& other) const noexcept(noexcept(NOEXCEPT_THIS operator==(other)))
	{
		return !operator==(other);
	}

	template<typename U, typename = _i6l::enable_if_t<_i6l::is_same_v<const T, const U>>>
	bool operator!=(const Array<U>& other) const noexcept(noexcept(NOEXCEPT_THIS operator==(other)))
	{
		return !operator==(other);
	}

#undef NOEXCEPT_THIS

	/**
	Helper function to compare array with `std::initializer_list`.

	One have to write either `arr.equals({ 1, 2, 3 })` or `arr == array({ 1, 2, 3 })` because
	`arr == { 1, 2, 3 }` is not syntactically valid as `std::initializer_list` object can't be
	created in this context.

	See http://en.cppreference.com/w/cpp/utility/initializer_list for details.
	*/
	bool equals(std::initializer_list<T> initializerList) const noexcept(_i6l::Elementwise<const T>::equalNothrow)
	{
		return _i6l::Elementwise<const T>::equal
		(
			_ptr,
			_length,
			initializerList.begin(),
			_i6l::checkedElementCountAsSigned(initializerList.size())
		);
	}

	// Slicing functions
	// --------------------------------------------------

	Array slice(const ssize startIndex, const ssize endIndex) noexcept
	{
		_i6l::checkSliceIndices(startIndex, endIndex, _length);
		return Array(_ptr + startIndex, endIndex - startIndex, _rc);
	}

	const Array slice(const ssize startIndex, const ssize endIndex) const noexcept
	{
		_i6l::checkSliceIndices(startIndex, endIndex, _length);
		return Array(_ptr + startIndex, endIndex - startIndex, _rc);
	}
};

template<typename T>
Array<T> array(std::initializer_list<T> initializerList) noexcept(noexcept(new(std::nothrow) Array<T>(initializerList)))
{
	return Array<T>(initializerList);
}


#ifdef UNSTD_UNITTEST // Unittests for `Array`

template<typename T>
void unittest_Array_construction_template()
{
	using ArrI = Array<T>;

	// Check unqualified implicit construction from "null" and list initialization:

	STATIC_ASSERT(noexcept(ArrI(nullptr)));
	STATIC_ASSERT(noexcept(ArrI({})));

	{
		ArrI arr = nullptr;
		assert(arr.ptr() == nullptr && arr.length() == 0);
	}
	{
		ArrI arr = {};
		assert(arr.ptr() != nullptr && arr.length() == 0);
	}
	{
		ArrI arr = { 10 };
		assert(arr.ptr() != nullptr && arr.length() == 1);
		assert(*arr.ptr() == 10);
	}
	{
		ArrI arr = { 10, 20, 30 };
		assert(arr.ptr() != nullptr && arr.length() == 3);
		const int* ptr = arr.ptr();
		assert(ptr[0] == 10 && ptr[1] == 20 && ptr[2] == 30);
	}
}

UNITTEST_BEGIN // Array construction
{
	using ArrI = Array<int>;

	// There is no default constructor:
	{
		ASSERT_NOT_COMPILES(ArrI arr);
	}

	unittest_Array_construction_template<int>();
	unittest_Array_construction_template<const int>();

	// Check unqualified copy construction:
	{
		ArrI arrNull = nullptr;
		ArrI arrEmpty = {};
		ArrI arrSingle = { 10 };
		ArrI arrThree = { 10, 20, 30 };

		for(ArrI* arrPtr : { &arrNull, &arrEmpty, &arrSingle, &arrThree })
		{
			ArrI& arr = *arrPtr;
			ArrI arrTest = arr;
			NOTHROW_ASSERT(arrTest.is(arr));
		}
	}

	// Check copy construction with `const`:
	using ArrCI = Array<const int>;
	{
		const ArrI constArr = { 10 };

		// Have to make both constructions invalid as there is no way
		// to know if constructing object is `const` in constructor:
		ASSERT_NOT_COMPILES(ArrI arr2 = constArr);
		ASSERT_NOT_COMPILES(const ArrI constArr2 = constArr);

		ArrCI arrConst = constArr;
		const ArrCI constArrConst = arrConst;
		ArrCI arrConst2 = constArrConst;

		assert(constArr.is(arrConst));
		assert(constArr.is(constArrConst));
		assert(constArr.is(arrConst2));
	}

	// Check moving construction:
	{
		ArrI srcArr = { 10 };
		ArrI arr = srcArr;
		STATIC_ASSERT(noexcept(ArrI(std::move(arr))));
		ArrI movedArr = std::move(arr);
		assert(arr.is(nullptr));
		assert(movedArr.is(srcArr));
	}
	{
		ArrI srcArr = { 10 };
		ArrI arr = srcArr;
		STATIC_ASSERT(noexcept(ArrCI(std::move(arr))));
		ArrCI movedArrConst = std::move(arr);
		assert(arr.is(nullptr));
		assert(movedArrConst.is(srcArr));
	}
	{
		ArrI srcArr = { 10 };
		ArrCI arrConst = srcArr;
		STATIC_ASSERT(noexcept(ArrCI(std::move(arrConst))));
		ArrCI movedArrConst = std::move(arrConst);
		assert(arrConst.is(nullptr));
		assert(movedArrConst.is(srcArr));

		ASSERT_NOT_COMPILES(ArrI movedArr = std::move(arrConst));
	}
}
UNITTEST_END

UNITTEST_BEGIN // Exception during array construction 
{
	struct Exception
	{
	};

	static std::string log;

	struct S
	{
		int n;
		bool isCopy = false;

		S(int n_) :
			n(n_)
		{
			log += 'n';
			log += '0' + char(n);
			log += ' ';
		}

		S(const S& s) :
			n(s.n),
			isCopy(true)
		{
			if(n == 4)
				log += 'e';
			log += 'c';
			log += '0' + char(n);
			log += ' ';

			if(n == 4)
				throw Exception{};
		}

		~S()
		{
			log += 'd';
			if(isCopy)
				log += 'c';
			log += '0' + char(n);
			log += ' ';
		}
	};

	STATIC_ASSERT(noexcept(Array<S>(nullptr)));
	STATIC_ASSERT(!noexcept(Array<S>({})));

	{
		Array<S> arr = { S{ 1 } };
		assert(log == "n1 c1 d1 ");
	}
	assert(log == "n1 c1 d1 dc1 ");
	log.clear();

	{
		Array<S> arr = { S{ 1 }, S{ 2 }, S{ 3 } };
		assert(log == "n1 n2 n3 c1 c2 c3 d3 d2 d1 ");
	}
	assert(log == "n1 n2 n3 c1 c2 c3 d3 d2 d1 dc3 dc2 dc1 ");
	log.clear();

	try
	{
		Array<S> arr = { S{ 1 }, S{ 2 }, S{ 3 }, S{ 4 }, S{ 5 } };
		assert(0);
	}
	catch(const Exception&)
	{
		assert(log == "n1 n2 n3 n4 n5 c1 c2 c3 ec4 dc3 dc2 dc1 d5 d4 d3 d2 d1 ");
	}
}
UNITTEST_END

UNITTEST_BEGIN // Exception during array destruction
{
	struct Exception
	{
	};

	static std::string log;

	struct S
	{
		int n;
		bool isCopy = false;

		S(int n_) :
			n(n_)
		{
			log += 'n';
			log += '0' + char(n);
			log += ' ';
		}

		S(const S& s) :
			n(s.n),
			isCopy(true)
		{
			log += 'c';
			log += '0' + char(n);
			log += ' ';
		}

		~S() noexcept(false)
		{
			if(isCopy && n == 3)
				log += 'e';
			log += 'd';
			if(isCopy)
				log += 'c';
			log += '0' + char(n);
			log += ' ';

			if(isCopy && n == 3)
				throw Exception{};
		}
	};

	STATIC_ASSERT(noexcept(new(std::nothrow) Array<S>(nullptr)));
	STATIC_ASSERT(!noexcept(new(std::nothrow) Array<S>({})));

	try
	{
		Array<S> arr = { S{ 1 }, S{ 2 }, S{ 3 }, S{ 4 }, S{ 5 } };
		assert(log == "n1 n2 n3 n4 n5 c1 c2 c3 c4 c5 d5 d4 d3 d2 d1 ");
		log.clear();
		arr = nullptr;
		assert(0);
	}
	catch(const Exception&)
	{
		assert(log == "dc5 dc4 edc3 dc2 dc1 ");
	}
}
UNITTEST_END

UNITTEST_BEGIN // Array assignment
{
	using ArrI = Array<int>;

	// Check unqualified assignment:
	{
		ArrI arr = { 1 };

		NOTHROW_DO(arr = nullptr);
		assert(arr.is(nullptr));

		NOTHROW_DO(arr = {});
		assert(arr.equals({}));

		arr = { 10, 20, 30 };
		assert(arr.equals({ 10, 20, 30 }));
	}
	{
		ArrI arrNull = nullptr;
		ArrI arrEmpty = {};
		ArrI arrSingle = { 10 };
		ArrI arrThree = { 10, 20, 30 };

		for(ArrI* arrPtr1 : { &arrNull, &arrEmpty, &arrSingle, &arrThree })
		{
			ArrI& arr1 = *arrPtr1;
			for(ArrI* arrPtr2 : { &arrNull, &arrEmpty, &arrSingle, &arrThree })
			{
				ArrI& arr2 = *arrPtr2;
				ArrI arrTest = arr1;
				NOTHROW_DO(arrTest = arr2);
				assert(arrTest.is(arr2));
			}
		}
	}

	// Check assignment with `const`:
	using ArrCI = Array<const int>;
	{
		const ArrI constArr = { 10 };
		ArrCI arrConst = nullptr;
		ArrCI arrConst2 = nullptr;

		NOTHROW_DO(arrConst = constArr);
		const ArrCI constArrConst = arrConst;
		NOTHROW_DO(arrConst2 = constArrConst);

		assert(constArr.is(arrConst));
		assert(constArr.is(constArrConst));
		assert(constArr.is(arrConst2));

		ArrI arr = nullptr;
		ASSERT_NOT_COMPILES(arr = constArr);
		ASSERT_NOT_COMPILES(arr = arrConst);
	}

	// Check rvalue assignment:
	{
		ArrI arr = nullptr;

		ASSERT_NOT_COMPILES(ArrI{ nullptr } = nullptr);
		ASSERT_NOT_COMPILES(ArrI{ nullptr } = {});
		ASSERT_NOT_COMPILES(ArrI{ nullptr } = arr);
		ASSERT_NOT_COMPILES(ArrCI{ nullptr } = arr);
		ASSERT_NOT_COMPILES(ArrCI{ nullptr } = ArrI{ nullptr });
	}
}
UNITTEST_END

UNITTEST_BEGIN // Array properties
{
	Array<int> arr = { 10 };
	const Array<int> constArr = { 10 };
	Array<const int> arrConst = { 10 };

	NOTHROW_ASSERT(arr.ptr() != nullptr && arr.length() == 1);
	NOTHROW_ASSERT(constArr.ptr() != nullptr && constArr.length() == 1);
	NOTHROW_ASSERT(arrConst.ptr() != nullptr && arrConst.length() == 1);

	STATIC_ASSERT((_i6l::is_same_v<decltype(arr.ptr()), int*>));
	STATIC_ASSERT((_i6l::is_same_v<decltype(constArr.ptr()), const int*>));
	STATIC_ASSERT((_i6l::is_same_v<decltype(arrConst.ptr()), const int*>));

}
UNITTEST_END

UNITTEST_BEGIN // Array element access
{
	Array<int> arr = { 10, 20 };
	const Array<int> constArr = arr;
	Array<const int> arrConst = arr;

	NOTHROW_ASSERT(arr[0] == 10 && arr[1] == 20);
	NOTHROW_ASSERT(&constArr[0] == &arr[0] && &constArr[1] == &arr[1]);
	NOTHROW_ASSERT(&arrConst[0] == &arr[0] && &arrConst[1] == &arr[1]);

	STATIC_ASSERT((_i6l::is_same_v<decltype(arr[0]), int&>));
	STATIC_ASSERT((_i6l::is_same_v<decltype(constArr[0]), const int&>));
	STATIC_ASSERT((_i6l::is_same_v<decltype(arrConst[0]), const int&>));
}
UNITTEST_END

UNITTEST_BEGIN // Array equality check
{
	using ArrI = Array<int>;

	ArrI arrNull1 = nullptr;
	ArrI arrNull2 = nullptr;
	ArrI arrEmpty1 = {};
	ArrI arrEmpty2 = {};
	ArrI arrSingle1 = { 10 };
	ArrI arrSingle2 = { 10 };
	ArrI arrSingleDifferent = { 11 };
	ArrI arrThree1 = { 10, 20, 30 };
	ArrI arrThree2 = { 10, 20, 30 };
	ArrI arrThreeDifferent = { 10, 20, 31 };

	auto is = [](const ArrI& arr1, const ArrI& arr2)
	{
		const bool is_ = arr1.is(arr2);
		NOTHROW_ASSERT(is_ == arr2.is(arr1));
		return is_;
	};

	auto eq = [](const ArrI& arr1, const ArrI& arr2)
	{
		const bool eq_ = arr1 == arr2;
		NOTHROW_ASSERT(eq_ == (arr2 == arr1));
		NOTHROW_ASSERT(eq_ == !(arr1 != arr2));
		assert(eq_ == !(arr2 != arr1));
		return eq_;
	};

	for(ArrI* arr1Ptr :
	{
		&arrNull1, &arrNull2,
		&arrEmpty1, &arrEmpty2,
		&arrSingle1, &arrSingle2,
		&arrThree1, &arrThree2
	})
	{
		ArrI& arr1 = *arr1Ptr;

		assert(eq(arr1, arr1) && is(arr1, arr1));

		for(ArrI* arr2Ptr :
		{
			&arrNull1, &arrNull2,
			&arrEmpty1, &arrEmpty2,
			&arrSingle1, &arrSingle2,
			&arrThree1, &arrThree2
		})
		{
			ArrI& arr2 = *arr2Ptr;

			const bool hasSameLength = arr1.length() == arr2.length();
			assert(eq(arr1, arr2) == hasSameLength);

			const bool isSame = &arr1 == &arr2 ||
				arr1.ptr() == nullptr && arr2.ptr() == nullptr;
			assert(is(arr1, arr2) == isSame);
		}

		assert(!eq(arr1, arrSingleDifferent));
		assert(!eq(arr1, arrThreeDifferent));
	}

	for(auto& emptyArr1 :
	{
		arrNull1, arrNull2,
		arrEmpty1, arrEmpty2,
	})
	{
		for(auto& emptyArr2 :
		{
			arrNull1, arrNull2,
			arrEmpty1, arrEmpty2,
		})
		{
			assert(eq(emptyArr1, emptyArr2));
		}
	}

	NOTHROW_ASSERT(arrNull1.equals({}));
	NOTHROW_ASSERT(arrEmpty1.equals({}));
	NOTHROW_ASSERT(arrSingle1.equals({ 10 }));
	NOTHROW_ASSERT(arrThree1.equals({ 10, 20, 30 }));
	NOTHROW_ASSERT(arrThree1 == array({ 10, 20, 30 }));

	{
		ArrI arr = nullptr;
		ASSERT_NOT_COMPILES(arr.equals(nullptr));
		ASSERT_NOT_COMPILES(arr.equals(arr));

		ASSERT_NOT_COMPILES(arr == nullptr);
		ASSERT_NOT_COMPILES(arr != nullptr);
	}
}
UNITTEST_END

UNITTEST_BEGIN // `const` qualified array equality 
{
	using ArrI = Array<int>;
	using ArrCI = Array<const int>;

	ArrI arr = { 10 };
	const ArrI cArr = arr;
	ArrCI arrC = arr;
	const ArrCI cArrC = arr;

#define CHECK_OP_ARRAYS(prefix, op, arr1, arr2, arr3, arr4) \
	NOTHROW_ASSERT(prefix (arr1 op (arr1))); \
	NOTHROW_ASSERT(prefix (arr1 op (arr2))); \
	NOTHROW_ASSERT(prefix (arr1 op (arr3))); \
	NOTHROW_ASSERT(prefix (arr1 op (arr4)))

#define CHECK_OP(prefix, op) \
	CHECK_OP_ARRAYS(prefix, op, arr, cArr, arrC, cArrC); \
	CHECK_OP_ARRAYS(prefix, op, cArr, arr, arrC, cArrC); \
	CHECK_OP_ARRAYS(prefix, op, arrC, arr, cArr, cArrC); \
	CHECK_OP_ARRAYS(prefix, op, cArrC, arr, cArr, arrC)

	CHECK_OP(, .is);
	CHECK_OP(, ==);
	CHECK_OP(!, !=);

#undef CHECK_OP
#undef CHECK_OP_ARRAYS
}
UNITTEST_END

UNITTEST_BEGIN // Array slicing
{
	Array<int> arr{ 10, 20, 30, 10, 20 };

	NOTHROW_ASSERT(arr.slice(0, 2) == array({ 10, 20 }));
	NOTHROW_ASSERT(arr.slice(0, 1) == array({ 10 }));
	NOTHROW_ASSERT(arr.slice(1, 2) == array({ 20 }));

	NOTHROW_ASSERT(arr.slice(0, 0).length() == 0);
	NOTHROW_ASSERT(arr.slice(5, 5).length() == 0);

	NOTHROW_ASSERT(!arr.slice(0, 2).is(arr.slice(3, 5)));
	NOTHROW_ASSERT(arr.slice(0, 2) == arr.slice(3, 5));

	Array<const int> arrConst = arr;
	const Array<int> constArr = arr;
	NOTHROW_ASSERT(arrConst.slice(1, 2).is(arr.slice(1, 2)));
	NOTHROW_ASSERT(constArr.slice(1, 2).is(arr.slice(1, 2)));
}
UNITTEST_END

#endif // UNSTD_UNITTEST

}
