/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include "common.hpp"
#include "exception.hpp"
#include "utils.hpp"


namespace unstd
{
namespace _i6l
{

// Bounds checking functions
// --------------------------------------------------

inline void enforceInBounds(const bool val, const char* errorMessage) noexcept
{
	if(!val)
		onOutOfBoundsError(errorMessage);
}

inline ssize checkedElementCountAsSigned(const size_t unsignedSize) noexcept
{
	enforceInBounds(canConvertSizeToSigned(unsignedSize), "element count can't be represented as `ssize`");
	return ssize(unsignedSize);
}

inline void checkElementIndex(const ssize index, const ssize length) noexcept
{
	enforceInBounds(index >= 0, "element index is negative");
	enforceInBounds(index < length, "element index is bigger than the last element index");
}

inline void checkSliceIndices(const ssize startIndex, const ssize endIndex, const ssize length) noexcept
{
	enforceInBounds(startIndex >= 0, "slice start index is negative");
	enforceInBounds(endIndex >= 0, "slice end index is negative");
	enforceInBounds(endIndex >= startIndex, "slice end index is less than start index");
	enforceInBounds(endIndex <= length, "slice end index is bigger than collection length");
}

}
}
