/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <cstring> // for `memcmp`, `memcpy`
#include <exception> // for `terminate`

#include "common.hpp"
#include "type_traits.hpp"
#include "utils.hpp"


namespace unstd
{
namespace _i6l
{

// Element-wise implementation namespace
// --------------------------------------------------

namespace _elementwise_impl
{

// Common functions for element-wise implementation
// --------------------------------------------------

template<typename T>
size_t bufferSizeUnsigned(const ssize count) noexcept
{
	const size_t bufferSize = ssizeAsUnsigned(count) * sizeof(T);
	INTERNAL_UNSTD_ASSERT(bufferSize / sizeof(T) == size_t(count));
	return bufferSize;
}


// Element-wise copy construction
// --------------------------------------------------

namespace copyConstruct
{

enum struct Kind
{
	trivial,
	nothrow,
	throwing,
	no,
};

template<typename T>
constexpr Kind kindOf = is_trivially_copy_constructible_v<T> ? Kind::trivial :
	is_nothrow_copy_constructible_v<T> ? Kind::nothrow :
	is_copy_constructible_v<T> ? Kind::throwing :
	Kind::no;

template<typename T, Kind = kindOf<T>>
struct Impl;

// Note: This specialization is intended for error message only.
template<typename T>
struct Impl<T, Kind::no>
{
	static void call
	(
		T* const,
		const T* const source,
		const ssize
	)
	{
		// Note: The statement below is for error
		// message only and will never execute:

		T copy(source[0]); // Will fail to compile because `T` is not copy constructible.
	}
};

template<typename T>
struct Impl<T, Kind::throwing>
{
	static void call
	(
		T* const dest,
		const T* const source,
		const ssize count
	)
	{
		ssize i = 0;
		try
		{
			for(; i < count; ++i)
				new (&dest[i]) T(source[i]);
		}
		catch(...)
		{
			// Perform cleanup on exception:

			for(--i; i >= 0; --i)
			{
				try
				{
					dest[i].~T();
				}
				catch(...)
				{
					std::terminate(); // Abort on second exception.
				}
			}

			throw;
		}
	}
};

template<typename T>
struct Impl<T, Kind::nothrow>
{
	static void call
	(
		T* const dest,
		const T* const source,
		const ssize count
	) noexcept
	{
		for(ssize i = 0; i < count; ++i)
			new (&dest[i]) T(source[i]);
	}
};

template<typename T>
struct Impl<T, Kind::trivial>
{
	static void call
	(
		T* const dest,
		const T* const source,
		const ssize count
	) noexcept
	{
		std::memcpy(dest, source, bufferSizeUnsigned<T>(count));
	}
};

} // namespace copyConstruct


// Element-wise destruction
// --------------------------------------------------

namespace destruct
{

enum struct Kind
{
	trivial,
	nothrow,
	throwing,
	no,
};

template<typename T>
constexpr Kind kindOf = is_trivially_destructible_v<T> ? Kind::trivial :
	is_nothrow_destructible_v<T> ? Kind::nothrow :
	is_destructible_v<T> ? Kind::throwing :
	Kind::no;

template<typename T, Kind = kindOf<T>>
struct Impl;

// Note: This specialization is intended for error message only.
template<typename T>
struct Impl<T, Kind::no>
{
	static void call
	(
		T* const ptr,
		const ssize
	)
	{
		// Note: The statement below is for error
		// message only and will never execute:

		ptr[0].~T(); // Will fail to compile because `T` is not destructible.
	}
};

template<typename T>
struct Impl<T, Kind::throwing>
{
	static void call
	(
		T* const ptr,
		const ssize count
	)
	{
		ssize i = count;
		try
		{
			for(--i; i >= 0; --i)
			{
				ptr[i].~T();
			}
		}
		catch(...)
		{
			// Perform cleanup on exception:

			for(--i; i >= 0; --i)
			{
				try
				{
					ptr[i].~T();
				}
				catch(...)
				{
					std::terminate(); // Abort on second exception.
				}
			}

			throw;
		}
	}
};

template<typename T>
struct Impl<T, Kind::nothrow>
{
	static void call
	(
		T* const ptr,
		const ssize count
	) noexcept
	{
		for(ssize i = count - 1; i >= 0; --i)
		{
			ptr[i].~T();
		}
	}
};

template<typename T>
struct Impl<T, Kind::trivial>
{
	static void call
	(
		T* const,
		const ssize
	) noexcept
	{
		// Do nothing.
	}
};

} // namespace destruct


// Element-wise equality comparison
// --------------------------------------------------

namespace equal
{

enum struct Kind
{
	trivial,
	nothrow,
	throwing,
	no,
};

template<typename T>
constexpr Kind kindOf = is_trivially_equality_comparable_v<const T> ? Kind::trivial :
	is_nothrow_equality_comparable_v<const T> ? Kind::nothrow :
	is_equality_comparable_v<const T> ? Kind::throwing :
	Kind::no;

template<typename T, Kind = kindOf<T>>
struct Impl;

// Note: This specialization is intended for error message only.
template<typename T>
struct Impl<T, Kind::no>
{
	static void call
	(
		T* const ptr1,
		T* const ptr2,
		const ssize
	)
	{
		// Note: The statement below is for error
		// message only and will never execute:

		bool eq = ptr1[0] == ptr2[0]; // Will fail to compile because `T` is not equality comparable.
	}
};

template<typename T>
struct Impl<T, Kind::throwing>
{
	static bool call
	(
		T* const ptr1,
		T* const ptr2,
		const ssize count
	)
	{
		for(ssize i = 0; i < count; ++i)
		{
			if(!(ptr1[i] == ptr2[i]))
				return false;
		}

		return true;
	}
};

template<typename T>
struct Impl<T, Kind::nothrow>
{
	static bool call
	(
		T* const ptr1,
		T* const ptr2,
		const ssize count
	) noexcept
	{
		for(ssize i = 0; i < count; ++i)
		{
			if(!(ptr1[i] == ptr2[i]))
				return false;
		}

		return true;
	}
};

template<typename T>
struct Impl<T, Kind::trivial>
{
	static bool call
	(
		const T* const ptr1,
		const T* const ptr2,
		const ssize count
	) noexcept
	{
		return std::memcmp(ptr1, ptr2, bufferSizeUnsigned<T>(count)) == 0;
	}
};

} // namespace equal

} // namespace _elementwise_impl


// `Elementwise` interface structure
// --------------------------------------------------

namespace
{

namespace impl = _elementwise_impl;

template<typename T>
struct Elementwise
{
private:

	static bool buffersOverlap
	(
		const T* const ptr1, const ssize count1,
		const T* const ptr2, const ssize count2
	)
	{
		return ptr1 + count1 > ptr2 && ptr2 + count2 > ptr1;
	}

public:

	static constexpr bool copyConstructNothrow = impl::copyConstruct::kindOf<T> != impl::copyConstruct::Kind::throwing;

	static void copyConstruct
	(
		T* const dest,
		const T* const source,
		const ssize count
	) noexcept(copyConstructNothrow)
	{
		INTERNAL_UNSTD_ASSERT(!buffersOverlap(dest, count, source, count));
		impl::copyConstruct::Impl<T>::call(dest, source, count);
	}

	static constexpr bool destructNothrow = impl::destruct::kindOf<T> != impl::destruct::Kind::throwing;

	static void destruct
	(
		T* const ptr,
		const ssize count
	) noexcept(destructNothrow)
	{
		impl::destruct::Impl<T>::call(ptr, count);
	}

	static constexpr bool equalNothrow = impl::equal::kindOf<T> != impl::equal::Kind::throwing;

	// An object is assumed to be equal to itself.
	static bool equal
	(
		T* const ptr1, const ssize count1,
		T* const ptr2, const ssize count2
	) noexcept(equalNothrow)
	{
		if(count1 != count2)
			return false;

		if(ptr1 == ptr2 || count1 == 0)
			return true;

		return impl::equal::Impl<T>::call(ptr1, ptr2, count1);
	}
};

}

}
}
