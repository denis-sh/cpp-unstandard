/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <cstdio> // for `fprintf`, `stderr`
#include <exception> // for `terminate`


namespace unstd
{
namespace _i6l
{

// Error callbacks
// --------------------------------------------------

#ifndef UNSTD_UNSAFE_NO_BOUNDS_CHECK

[[noreturn]] inline void onOutOfBoundsError(const char* errorMessage) noexcept
{
	std::fprintf(stderr, "Out of bounds error: %s.\n", errorMessage);
	std::terminate();
}

#else

inline void onOutOfBoundsError(const char* errorMessage) noexcept
{ }

#endif


[[noreturn]] inline void onOutOfMemoryError() noexcept
{
	std::fprintf(stderr, "Out of memory error.\n");
	std::terminate();
}

}
}
