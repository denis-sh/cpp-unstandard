/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <cstddef> // for `max_align_t`
#include <algorithm> // for `swap`
#include <new> // for new/delete operators and `nothrow`

#include "common.hpp"
#include "exception.hpp"
#include "utils.hpp"


namespace unstd
{
namespace _i6l
{

template<typename Prefix, typename Element>
struct PrefixedBuffer
{
	static_assert(alignof(Prefix) <= alignof(std::max_align_t), "`Prefix` has unsupported alignment.");
	static_assert(alignof(Element) <= alignof(std::max_align_t), "`Element` has unsupported alignment.");

private:

	void* _ptr = nullptr;

public:

	bool allocated() const noexcept
	{
		return _ptr != nullptr;
	}

	struct AllocateResult
	{
		Prefix* prefix;
		Element* buffer;
	};

	AllocateResult allocate(const ssize count) noexcept
	{
		INTERNAL_UNSTD_ASSERT(!allocated());
		INTERNAL_UNSTD_ASSERT(count >= 0);

		const size_t buffOffset = count > 0 ?
			alignUpFor<Element>(sizeof(Prefix)) : 0;

		const size_t allocationSize = count > 0 ?
			buffOffset + sizeof(Element) * ssizeAsUnsigned(count) : sizeof(Prefix);

		_ptr = ::operator new[](allocationSize, std::nothrow);

		if(_ptr == nullptr)
			onOutOfMemoryError();

		return
		{
			/*prefix:*/ reinterpret_cast<Prefix*>(_ptr),
			/*buffer:*/ reinterpret_cast<Element*>(reinterpret_cast<char*>(_ptr) + buffOffset),
		};
	}

	void free() noexcept
	{
		INTERNAL_UNSTD_ASSERT(allocated());

		::operator delete[](_ptr);
		_ptr = nullptr;
	}

	void swap(PrefixedBuffer& other) noexcept
	{
		std::swap(_ptr, other._ptr);
	}

	Prefix& prefix() noexcept
	{
		return *reinterpret_cast<Prefix*>(_ptr);
	}
};

}
}
