/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <type_traits> // for `true_type`, `false_type`, `{void,conditional,enable_if,remove_const}_t`, and 9 `is_*_v` traits
#include <utility> // for `declval`


namespace unstd
{
namespace _i6l
{

// `std` traits added after C++11
// --------------------------------------------------

#if defined(__cpp_lib_void_t) && __cpp_lib_void_t >= 201411

using std::void_t; // since C++17

#else

template<typename...>
using void_t = void;

#endif


#if __cplusplus > 201103L

// since C++14:
using std::conditional_t; 
using std::enable_if_t;
using std::remove_const_t;

#else

template<bool cond, typename T, typename F>
using conditional_t = typename std::conditional<cond, T, F>::type;

template<bool cond, typename T = void>
using enable_if_t = typename std::enable_if<cond, T>::type;

template<typename T>
using remove_const_t = typename std::remove_const<T>::type;

#endif


#if defined(__cpp_lib_type_trait_variable_templates) && __cpp_lib_type_trait_variable_templates >= 201510

// since C++17:

using std::is_same_v;
using std::is_scalar_v;
using std::is_convertible_v;

using std::is_copy_constructible_v; 
using std::is_trivially_copy_constructible_v;
using std::is_nothrow_copy_constructible_v;

using std::is_destructible_v;
using std::is_trivially_destructible_v;
using std::is_nothrow_destructible_v;

#else

template<typename T, typename U>
constexpr bool is_same_v = std::is_same<T, U>::value;

template<typename T>
constexpr bool is_scalar_v = std::is_scalar<T>::value;

template<typename From, typename To>
constexpr bool is_convertible_v = std::is_convertible<From, To>::value;


template<typename T>
constexpr bool is_copy_constructible_v = std::is_copy_constructible<T>::value;

template<typename T>
constexpr bool is_trivially_copy_constructible_v = std::is_trivially_copy_constructible<T>::value;

template<typename T>
constexpr bool is_nothrow_copy_constructible_v = std::is_nothrow_copy_constructible<T>::value;


template<typename T>
constexpr bool is_destructible_v = std::is_destructible<T>::value;

template<typename T>
constexpr bool is_trivially_destructible_v = std::is_trivially_destructible<T>::value;

template<typename T>
constexpr bool is_nothrow_destructible_v = std::is_nothrow_destructible<T>::value;

#endif


// `is_*equality_comparable` traits
// --------------------------------------------------

namespace
{

using std::declval;
using std::true_type;
using std::false_type;

template<typename T, typename = void>
struct is_equality_comparable : false_type
{ };

template<typename T>
struct is_equality_comparable<T,
	void_t<decltype(declval<T&>() == declval<T&>())>
> : conditional_t<
	is_convertible_v<decltype(declval<T&>() == declval<T&>()), bool>,
	true_type,
	false_type>
{ };

template<typename T>
constexpr bool is_equality_comparable_v = is_equality_comparable<T>::value;

template<typename T, typename = void>
struct is_nothrow_equality_comparable : false_type
{ };

template<typename T>
struct is_nothrow_equality_comparable<T,
	void_t<decltype(declval<T&>() == declval<T&>())>
> : conditional_t<
	is_equality_comparable_v<T> && noexcept(declval<T&>() == declval<T&>()),
	true_type,
	false_type>
{ };

template<typename T>
constexpr bool is_nothrow_equality_comparable_v = is_nothrow_equality_comparable<T>::value;

template<typename T>
constexpr bool is_trivially_equality_comparable_v = is_scalar_v<T>;

}

}
}
