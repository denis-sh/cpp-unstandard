/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once

#include <limits> // for `numeric_limits`

#include "common.hpp"


namespace unstd
{
namespace _i6l
{

// `ssize` <-> `size_t` conversion functions
// --------------------------------------------------

constexpr bool canConvertSizeToSigned(const size_t unsignedSize) noexcept
{
	return unsignedSize <= size_t(std::numeric_limits<ssize>::max());
}

inline ssize sizeAsSigned(const size_t unsignedSize) noexcept
{
	INTERNAL_UNSTD_ASSERT(canConvertSizeToSigned(unsignedSize));
	return ssize(unsignedSize);
}

inline size_t ssizeAsUnsigned(const ssize signedSize) noexcept
{
	INTERNAL_UNSTD_ASSERT(signedSize >= 0);
	return size_t(signedSize);
}


// Alignment functions
// --------------------------------------------------

template<typename T>
constexpr size_t alignUpFor(const size_t n) noexcept
{
#ifdef UNSTD_HAS_CXX14_CONSTEXPR // If has relaxing constexpr requirements

	constexpr size_t alignment = alignof(T); // `alignment` is a power of 2
	const size_t n2 = n + alignment - 1;
	const size_t n2AlignedDown = n2 & ~(alignment - 1); // alignment - 1: 0b11, 0b111, ...
	return n2AlignedDown; // `n2` aligned down is `n` aligned up.

#else // If no relaxing constexpr support

	// Same logic as above in one expression:
	return (n + alignof(T) - 1) & ~(alignof(T) - 1);

#endif
}

#ifdef UNSTD_UNITTEST // Unittests for `alignUpFor`

STATIC_UNITTEST
{
	using Align1 = char;
	struct alignas(2) Align2 { };
	struct alignas(4) Align4 { };
	struct alignas(8) Align8 { };

	STATIC_ASSERT(alignUpFor<Align1>(9) == 9);
	STATIC_ASSERT(alignUpFor<Align2>(9) == 10);
	STATIC_ASSERT(alignUpFor<Align4>(9) == 12);
	STATIC_ASSERT(alignUpFor<Align8>(9) == 16);
	STATIC_ASSERT(alignUpFor<Align8>(0) == 0);
	STATIC_ASSERT(alignUpFor<Align8>(1) == 8);
	STATIC_ASSERT(alignUpFor<Align8>(7) == 8);
	STATIC_ASSERT(alignUpFor<Align8>(8) == 8);
}

#endif // UNSTD_UNITTEST

}
}
