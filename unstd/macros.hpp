/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once


// Compiler properties macros
// --------------------------------------------------

#if defined(_MSC_VER) && !defined(__clang__)
#define UNSTD_VISUAL_CXX // Macro for Visual C++ compiler detection
#endif

#if defined(__GNUG__) && !defined(__clang__)
#define UNSTD_GNU_CXX // Macro for GNU C++ compiler detection
#endif


#if defined(UNSTD_VISUAL_CXX) ? \
	_MSC_FULL_VER >= /* VS 2015 Update 2 */ 190023918 : \
	defined(__cpp_variable_templates) && __cpp_variable_templates >= 201304
#define UNSTD_HAS_VARIABLE_TEMPLATES
#endif

#if defined(UNSTD_VISUAL_CXX) ? \
	_MSC_FULL_VER >= /* pre-release build after VS 2015 Update 3 */ 190024315 : \
	__cpp_constexpr >= 201304
#define UNSTD_HAS_CXX14_CONSTEXPR
#endif


// Comiler properties check
// --------------------------------------------------

#ifdef UNSTD_VISUAL_CXX

#ifndef UNSTD_HAS_VARIABLE_TEMPLATES
#error Unstandard library requires at least Visual C++ from Visual Studio 2015 Update 2 \
because support for variable templates from ISO C++ 2014 standard was added in this update.
#endif

#else

#if __cplusplus < 201103L
#error Unstandard library requires compiler and library support for the ISO C++ 2011 standard.
#endif

#ifndef UNSTD_HAS_VARIABLE_TEMPLATES
#error Unstandard library requires compiler support for variable templates from ISO C++ 2014 standard.
#endif

#endif


// Library debugging macros
// --------------------------------------------------

#if defined(UNSTD_UNITTEST) && !defined(UNSTD_DEBUG)
#define UNSTD_DEBUG
#endif

#ifdef UNSTD_DEBUG

#ifdef NDEBUG
#error Do not define `NDEBUG` when unittesting/debugging Unstandard library.
#endif

#include <cassert>

#define INTERNAL_UNSTD_ASSERT(expr) assert(expr)

#else

#define INTERNAL_UNSTD_ASSERT(expr)

#endif // UNSTD_DEBUG


// Library unittesting macros
// --------------------------------------------------

#ifdef UNSTD_UNITTEST

#define INTERNAL_UNSTD_CONCAT_UNEXPANDED(a, b) a ## b
#define INTERNAL_UNSTD_CONCAT(a, b) INTERNAL_UNSTD_CONCAT_UNEXPANDED(a, b)

#define STATIC_UNITTEST namespace INTERNAL_UNSTD_CONCAT(__unstd_static_unittest_, __LINE__)

#define UNITTEST_BEGIN static int INTERNAL_UNSTD_CONCAT(__unstd_unittest_, __LINE__) = (([]()
#define UNITTEST_END )(), 0);

#define STATIC_ASSERT(expr) static_assert(expr, #expr)

#define NOTHROW_DO(expr) do { STATIC_ASSERT(noexcept(expr)); expr; } while(0)
#define NOTHROW_ASSERT(expr) do { STATIC_ASSERT(noexcept(expr)); assert(expr); } while(0)

#define ASSERT_NOT_COMPILES(expr)

#endif // UNSTD_UNITTEST


// Various macros
// --------------------------------------------------

// Explicit marker for implicit constructors
#define UNSTD_IMPLICIT
