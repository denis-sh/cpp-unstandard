/*
Copyright: Denis Shelomovskii 2017

License: Boost License 1.0, see accompanying file or http://boost.org/LICENSE_1_0.txt
*/
#pragma once


namespace unstd
{

using size_t = decltype(sizeof(char));
using ssize = decltype(static_cast<char*>(0) - static_cast<char*>(0));

}
